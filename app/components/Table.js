import React from 'react';

export const Table = props => {
  const { table, sortByName, sortById } = props;
  return (
    <table width="100%">
      <tbody>
        <TableHead
          head={Object.keys(table[0]).sort()}
          sortById={sortById}
          sortByName={sortByName}
        />
        {table.map(row => (
          <TableRow row={row} />
        ))}
      </tbody>
    </table>
  );
};

const TableHead = props => {
  const { head } = props;
  return (
    <tr>
      {head.map(key => (
        <th>{key}</th>
      ))}
    </tr>
  );
};

const TableRow = props => {
  const { row } = props;
  row.address = !row.address
    ? ''
    : Object.entries(row.address).reduce(
        (prev, current) => `${prev}, ${current[0]} ${current[1]}`,
      );
  return (
    <tr align="center">
      {Object.keys(row)
        .sort()
        .map(key => (
          <td key>{row[key]}</td>
        ))}
    </tr>
  );
};
