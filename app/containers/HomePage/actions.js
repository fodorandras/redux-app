export function login(loginData) {
  return {
    type: 'LOGIN',
    payload: loginData,
  };
}

export function loginFailed() {
  return {
    type: 'LOGIN_FAILED',
  };
}
