/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import { login, loginFailed } from './actions';
import reducer from './reducer';
import { loginService } from './services';
import LoginForm from './loginForm';

/* eslint-disable react/prefer-stateless-function */
class HomePage extends React.PureComponent {
  render() {
    return (
      <LoginForm customError={this.props.error} onSubmit={this.props.onLogin} />
    );
  }
}

const mapStateToProps = state => ({
  ...state.toJS().home,
});

const mapDispatchToProps = dispatch => ({
  onLogin: formValues => {
    loginService(formValues)
      .then(data => data.json())
      .then(data => {
        if (data.success) {
          dispatch(login(data));
        } else {
          dispatch(loginFailed());
        }
      })
      .catch(() => dispatch(loginFailed()));
  },
});

const withReducer = injectReducer({ key: 'home', reducer });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withConnect,
)(HomePage);
