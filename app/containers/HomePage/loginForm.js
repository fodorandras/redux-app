import React from 'react';
import { Field, reduxForm } from 'redux-form/immutable';

const LoginForm = props => {
  const { customError, handleSubmit, submitting } = props;
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <Field
          id="userName"
          name="userName"
          component="input"
          type="text"
          placeholder="Username"
        />
      </div>
      <div>
        <Field
          id="password"
          name="password"
          component="input"
          type="password"
          placeholder="Password"
        />
      </div>
      {customError && <strong>{customError}</strong>}
      <div>
        <button type="submit" disabled={submitting}>
          Log In
        </button>
      </div>
    </form>
  );
};

export default reduxForm({
  form: 'loginForm',
})(LoginForm);
