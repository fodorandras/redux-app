import { fromJS } from 'immutable';

// The initial state of the App
export const initialState = fromJS({
  token: '',
  error: '',
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case 'LOGIN':
      return state.set('token', action.payload.token);
    case 'LOGIN_FAILED':
      return state.set('error', 'Login Failed');
    default:
      return state;
  }
}

export default homeReducer;
