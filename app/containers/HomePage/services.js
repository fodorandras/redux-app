export function loginService(loginData) {
  return fetch('http://localhost:8080/api/user/login', {
    method: 'POST',
    body: JSON.stringify(loginData),
    headers: {
      'Content-Type': 'application/json',
    },
  });
}
