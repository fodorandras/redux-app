export function fetchTable(table) {
  return {
    type: 'FETCH_TABLE',
    payload: table,
  };
}
