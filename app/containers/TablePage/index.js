import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import { tableService } from './services';
import { fetchTable } from './actions';
import { Table } from '../../components/Table';

/* eslint-disable react/prefer-stateless-function */
class TablePage extends React.PureComponent {
  componentDidMount() {
    this.props.fetchTable();
  }

  render() {
    const { table } = this.props;
    return (
      <div>
        {table.length > 0 ? (
          <Table table={table} />
        ) : (
          <img src="/ajax-loader.gif" alt="loading" />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state.toJS().table,
});

const mapDispatchToProps = dispatch => ({
  fetchTable: () => {
    tableService()
      .then(data => data.json())
      .then(data => dispatch(fetchTable(data)));
  },
});

const withReducer = injectReducer({ key: 'table', reducer });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withConnect,
)(TablePage);
