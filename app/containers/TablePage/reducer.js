import { fromJS } from 'immutable';

// The initial state of the App
export const initialState = fromJS({
  table: [],
});

function tableReducer(state = initialState, action) {
  switch (action.type) {
    case 'FETCH_TABLE':
      return state.set('table', action.payload);
    default:
      return state;
  }
}

export default tableReducer;
